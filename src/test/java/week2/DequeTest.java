package week2;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

/**
 * Created by minh on 11/10/16.
 */
public class DequeTest {

    @Test
    public void isEmpty() throws Exception {
        Deque<String> deck = new Deque<String>();
        assertTrue("should return true in new deck", deck.isEmpty());

        deck.addLast("one");
        assertFalse("should return false after add (last) an item", deck.isEmpty());
    }

    @Test
    public void size() throws Exception {
        Deque<String> deck = new Deque<String>();
        assertEquals("size should return 0 in new deck", 0, deck.size());

        deck.addLast("one");
        assertEquals("size should return 1 after and (last) an item", 1, deck.size());

    }

    @Test
    public void addFirst() throws Exception {
        Deque<String> deck = new Deque<String>();
        deck.addFirst("one");
        deck.addFirst("two");
        deck.addFirst("three");

        assertEquals("removeFirst should return 'three' in deck {'three', 'two', 'one'}", "three", deck.removeFirst());
        assertEquals("removeFirst should return 'two' in deck {'two', 'one'}", "two", deck.removeFirst());
        assertEquals("removeFirst should return 'one' in deck {'one'}", "one", deck.removeFirst());

        Deque<Integer> ideck = new Deque<Integer>();
        ideck.addFirst(0);
    }

    @Test
    public void addLast() throws Exception {
        Deque<String> deck = new Deque<String>();
        deck.addLast("three");
        deck.addLast("two");
        deck.addLast("one");

        assertEquals("removeLast should return 'one' in deck {'three', 'two', 'one'}", "one", deck.removeLast());
        assertEquals("removeLast should return 'two' in deck {'three', 'two'}", "two", deck.removeLast());
        assertEquals("removeLast should return 'three' in deck {'three'}", "three", deck.removeLast());
    }

    @Test(expected = NullPointerException.class)
    public void addNull() throws Exception {
        Deque<String> deck = new Deque<String>();
        try {
            deck.addLast(null);
            deck.addFirst(null);
        } catch (NullPointerException ex){
            assertNotNull("should throw NullPointerException when add null to deck", ex);
            throw ex;
        }
        fail("no NullPointerException thrown when add null to deck");
    }


    @Test(expected = NoSuchElementException.class)
    public void removeX() throws Exception {
        try {
            Deque<String> deck = new Deque<String>();
            deck.removeFirst();
            deck.removeLast();

        } catch (NoSuchElementException ex){
            assertNotNull("should throw NoSuchElementException when remove element from empty deck.");
            throw ex;
        }

        fail("NoSuchElementException did not thrown!");
    }

    @Test
    public void iterator() throws Exception {
        Deque<String> deck = new Deque<String>();
        Iterator<String> iterator = deck.iterator();

        assertFalse("hasNext should return false in new deck", iterator.hasNext());

        deck.addLast("one");
        assertTrue("hasNext should return true after add 'one'", iterator.hasNext());

        iterator.next();
        assertFalse("hasNext should return false after call next in deck {'one'}", iterator.hasNext());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void iteratorRemove() throws Exception {
        try
        {
            Deque<String> deck = new Deque<String>();
            Iterator<String> iterator = deck.iterator();

            deck.addLast("one");
            iterator.remove();
        }
        catch(UnsupportedOperationException e)
        {
            String message = "should throw UnsupportedOperationException when call remove";
            assertNotNull(message, e);
            throw e;
        }
        fail("UnsupportedOperationException did not throw!");
    }

    @Test(expected = NoSuchElementException.class)
    public void iteratorNextEx() throws Exception {
        try
        {
            Deque<String> deck = new Deque<String>();
            Iterator<String> iterator = deck.iterator();

            deck.addLast("one");
            iterator.next();
            iterator.next();
        }
        catch(NoSuchElementException e)
        {
            String message = "should throw NoSuchElementException when call next twice in one element deck iterator";
            assertNotNull(message, e);
            throw e;
        }
        fail("NoSuchElementException did not throw!");
    }

    @Test
    public void sumary() throws Exception {
        Deque<Integer> id = new Deque<Integer>();
//        id.addFirst(0);
//        id.addLast(1);
//        id.removeFirst();
//        id.removeLast();

        id.addFirst(0);
        id.addLast(1);
        id.addFirst(2);
        id.addFirst(3);

        id.removeLast();
       // System.out.println(id.removeFirst());

        for(int i : id){
            System.out.print("i: " + i);
        }
    }
}