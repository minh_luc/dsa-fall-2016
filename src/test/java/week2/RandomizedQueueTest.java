package week2;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by minh on 16/10/16.
 */
public class RandomizedQueueTest {
    @Test
    public void sumary(){
        RandomizedQueue<Integer> rq = new RandomizedQueue<Integer>();

        rq.enqueue(34);
        rq.enqueue(20);
        rq.enqueue(18);
        assertEquals("size should return 3", 3, rq.size());

        printIterable(rq);

        int dq1 = rq.dequeue();
        System.out.println("dequeue() ==> " + dq1);
        assertTrue("dequeue should one of {43,20, 18}", dq1 == 34 || dq1 == 20 || dq1 == 18);

        printIterable(rq);

        rq.enqueue(23);
        rq.size();
        assertEquals("size should return 3", 3, rq.size());

        rq.isEmpty();
        assertFalse("isEmpty should return false", rq.isEmpty());

        rq.enqueue(6);
        int dq2 = rq.dequeue();
        System.out.println("dequeue() ==> " + dq2);


        printIterable(rq);
    }

    @Test
    public void sample(){
        RandomizedQueue<Integer> iq = new RandomizedQueue<Integer>();

        iq.enqueue(0);
        assertEquals("sample should return 0 in queue {0}", 0, iq.sample().intValue());
    }

    @Test
    public void iterator(){
        RandomizedQueue<Integer> iq = new RandomizedQueue<Integer>();

        for(int i = 0; i < 10; i++){
            iq.enqueue(i);
        }
    }

    private void printIterable(Iterable list){
        for ( Object i : list ){
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
