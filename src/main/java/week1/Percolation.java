/**
 * Created by minh on 09/09/16.
 */

package week1;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

import java.util.Arrays;

public class Percolation {

    private static final char SITE_FULL = 2;
    private static final char SITE_OPEN = 1;
    private static final char SITE_BLOCK = 0;


    private WeightedQuickUnionUF myUF;
    private int topVituralRoot;
    private int bottomVituralRoot;
    private char[] grids;
    private int n;

    public Percolation(int n) {    // create n-by-n grid, with all sites blocked
        if (n <= 0) throw new IllegalArgumentException("site of grids must larger than 0");
        this.n = n;
        grids = new char[n * n];
        myUF = new WeightedQuickUnionUF(n * n + 2);
        topVituralRoot = n * n;
        bottomVituralRoot = n * n + 1;
        setAll(SITE_BLOCK);
    }

    private boolean inRange(final int i, final int j) {
        return (i >= 1) && (i <= n) && (j >= 1) && (j <= n);
    }

    private void setAll(final char status) {
        for (int i = 0; i < grids.length; i++) {
            grids[i] = status;
        }
    }

    private int getIndex(final int i, final int j) {
        if (!inRange(i, j)) throw new IndexOutOfBoundsException("index out of Bounds");
        return ((i - 1) * n) + (j - 1);
    }

    private void backFill(int i, int j) {
        if (!inRange(i, j)) return;
        int index = getIndex(i, j);
        if (!isOpen(index)) return;

        grids[index] = SITE_FULL;
        backFill(i - 1, j);
        backFill(i + 1, j);
        backFill(i, j - 1);
        backFill(i, j + 1);

    }

    private void surroundConnect(int p, int q) {
        int index = getIndex(p, q);
        boolean full = false;

        for (int i = 0; i < 4; i++) {
            int dp = 0;
            int dq = 0;
            boolean cond;

            if (i == 0) {
                dp = -1;
                dq = 0;
                cond = p > 1;
            } else if (i == 1) {
                dp = 1;
                dq = 0;
                cond = p < n;
            } else if (i == 2) {
                dp = 0;
                dq = 1;
                cond = q < n;
            } else {
                dp = 0;
                dq = -1;
                cond = q > 1;
            }

            if (cond && inRange(p + dp, q + dq) && grids[getIndex(p + dp, q + dq)] != SITE_BLOCK) {
                int neigbough = getIndex(p + dp, q + dq);
                myUF.union(index, neigbough);
                if (isFull(neigbough)) {
                    full = true;
                }
            }
        }

        if (p == 1) {
            myUF.union(index, topVituralRoot);
            full = true;
        }

        if (p == n) {
            myUF.union(index, bottomVituralRoot);
        }

        if (full) {
            backFill(p, q);
        }

    }

    private boolean isOpen(int index) {
        return grids[index] == SITE_OPEN;
    }

    private boolean isFull(int index) {
        return grids[index] == SITE_FULL;
    }

    // open site (row i, column j) if it is not open already
    public void open(int i, int j) {
        int index = getIndex(i, j);
        if (grids[index] != SITE_BLOCK) return;

        grids[index] = SITE_OPEN;
        surroundConnect(i, j);

    }

    public boolean isOpen(int i, int j) {    // is site (row i, column j) open?
        int index = getIndex(i, j);
        return isOpen(index) || isFull(index);
    }

    public boolean isFull(int i, int j) {     // is site (row i, column j) full?
        int index = getIndex(i, j);

        return grids[index] == SITE_FULL;
    }

    public boolean percolates() {             // does the system percolate?
        return (myUF.connected(topVituralRoot, bottomVituralRoot));
    }

    public static void main(String[] args) {  // test client (optional)
        Percolation p = new Percolation(6);
        p.open(1, 6);
        System.out.print(p.isOpen(1, 6));
    }

}
