package week1;

import edu.princeton.cs.algs4.StdRandom;

/**
 * Created by minh on 09/09/16.
 */
public class PercolationStats {
    private int n;
    private int trials;
    private double[] percolationThresholds;

    private double mean;
    private double stddev;

    // perform trials independent experiments on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) throw new IllegalArgumentException("n and trials must lager than 0");
        this.n = n;
        this.trials = trials;
        mean = -1.0;
        stddev = -1.0;

        this.percolationThresholds = new double[trials];
        for (int i = 0; i < percolationThresholds.length; i++) {
            percolationThresholds[i] = percolationThreshold();
        }

        mean = meanCalc();
        stddev = stddevCalc();
    }

    private double percolationThreshold() {
        Percolation p = new Percolation(n);
        int c = 0;
        while (!p.percolates()) {
            int i = StdRandom.uniform(n) + 1;
            int j = StdRandom.uniform(n) + 1;
            if (!p.isOpen(i, j)) {
                p.open(i, j);
                c++;
            }
        }

        return (double) (c) / (n * n);
    }

    private double meanCalc() {                          // sample mean of percolation threshold
        double s = 0;

        for (double p : percolationThresholds) {
            s += p;
        }
        return s / trials;
    }

    private double stddevCalc() {                       // sample standard deviation of percolation threshold
        double m = mean;
        double s = 0;
        for (double p : percolationThresholds) {
            s += (p - m) * (p - m);
        }

        if (s == 0) return 0;
        else return Math.sqrt(s / (trials - 1));
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stddev;
    }

    public double confidenceLo() {                  // low  endpoint of 95% confidence interval
        return (mean() - 1.96 * stddev() / Math.sqrt(trials));
    }

    public double confidenceHi() {                 // high endpoint of 95% confidence interval
        return (mean() + 1.96 * stddev() / Math.sqrt(trials));
    }

    public static void main(String[] args) {    // test client (described below)
        int n = 2;
        int t = 100;

        PercolationStats instance = new PercolationStats(n, t);
        System.out.printf("PercolationStats %d %d%n", n, t);
        System.out.printf("mean: %f%n", instance.mean());
        System.out.printf("stddev: %f%n", instance.stddev());
    }
}
