package week1;

/**
 * Created by minh on 09/09/16.
 */
public class UF {
    private int[] id;

    public UF(){
        this(0);
    }

    public UF(int n){
        if(n <= 0) n = 128;
        id = new int[n];
    }

    public void union(int p, int q){
        int rootP = root(p);
        int rootQ = root(q);

        id[rootP] = rootQ;
    }

    public boolean connected(int p, int q){
        return root(p) == root(q);
    }

    public int find(int p){
        return 0;
    }

    public int count(){
        return id.length;
    }

    private int root(int p){
        int i = p;
        while(i != id[i]){
            id[i] = id[id[i]];
            i = id[p];
        }

        return i;
    }
}
