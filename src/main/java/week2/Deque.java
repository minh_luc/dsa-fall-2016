package week2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by minh on 10/10/16.
 */
public class Deque<Item> implements Iterable<Item> {
    private Element first;
    private Element last;
    private int size;

    public Deque() {
        first = null;
        last = null;
        size = 0;
    }

    public boolean isEmpty() {
        return (size == 0);
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        if (item == null) throw new NullPointerException("can not add null value into deck");

        Element el = new Element();
        el.value = item;
        el.next = first;

        first = el;

        if (isEmpty()) {
            last = el;
            last.next = null;
            last.prev = null;
        } else {
            first.next.prev = first;
        }

        size++;
    }

    public void addLast(Item item) {
        if (item == null) throw new NullPointerException("can not add null value into deck");

        Element el = new Element();
        el.value = item;
        el.prev = last;

        last = el;

        if (isEmpty()) {
            first = last;
            first.next = null;
            first.prev = null;
        } else {
            last.prev.next = last;
        }

        size++;
    }

    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException("can not remove item from empty deck.");

        Item value = first.value;
        first = first.next;
        size--;

        if(size == 0) {
            first = null;
            last = null;
        } else {
            first.prev = null;
        }

        return value;
    }

    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException("can not remove item from empty deck.");

        Item value = last.value;
        last = last.prev;

        size--;

        if(size == 0) {
            first = null;
            last = null;
        } else {
            last.next = null;
        }

        return value;
    }

    public Iterator<Item> iterator() {
        return new MyIterator();
    }

    private class Element {
        Item value;
        Element next;
        Element prev;

        Element() {
            value = null;
            prev = null;
            next = null;
        }
    }

    private class MyIterator implements Iterator<Item> {
        private Element current;

        MyIterator() {
            super();
            current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            if (current == null) throw new NoSuchElementException("iterator has not next element");
            Item val = current.value;
            current = current.next;
            return val;
        }

        public void remove() {
            throw new UnsupportedOperationException("unsupported operation!");
        }
    }

    public static void main(String[] args) {
    }
}