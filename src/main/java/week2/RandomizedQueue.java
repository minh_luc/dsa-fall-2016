package week2;

import edu.princeton.cs.algs4.StdRandom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by minh on 11/10/16.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private Element root;
    private int size;

    // construct an empty randomized queue
    public RandomizedQueue() {
        root = null;
        size = 0;
    }

    // is the queue empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the queue
    public int size() {
        return size;
    }

    // add the item{
    public void enqueue(Item item) {
        if ( item == null ) throw new NullPointerException("can not add null value into queue");

        Element el = new Element();
        el.value = item;
        el.next = root;
        root = el;

        if ( !isEmpty() ) {
            root.next.prev = root;
        }
        size++;
    }

    // remove and return a random item
    public Item dequeue() {
        if ( isEmpty() ) throw new NoSuchElementException("can not remove item from empty queue.");

        Element el = getRandomElement();
        if ( el.prev != null ) {
            el.prev.next = el.next;
        } else{
            root = el.next;
        }

        if ( el.next != null ) {
            el.next.prev = el.prev;
        }

        size--;

        return el.value;
    }

    // return (but do not remove) a random item
    public Item sample() {
        if ( isEmpty() ) throw new NoSuchElementException("can not get item from empty deck.");

        return getRandomElement().value;
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new MyIterator(root);
    }

    private Element getRandomElement() {
        int pos = StdRandom.uniform(size);;

        Element el = root;

        for ( int i = 0; i < pos; i++ ) {
            el = el.next;
        }

        return el;
    }

    // unit testing
    public static void main(String[] args) {

    }

    private class Element {
        Item value;
        Element next;
        Element prev;

        Element() {
            value = null;
            prev = null;
            next = null;
        }
    }

    private class MyIterator implements Iterator<Item> {
        private Element current;
        ArrayList<Item> gots;

        MyIterator(Element r) {
            super();
            current = r;
            gots = new ArrayList<Item>();
        }

        public boolean hasNext() {
            return gots.size() < size();
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException("iterator has not next element");
            Item nextIt = null;
            do {
                nextIt = sample();
            } while ( gots.contains(nextIt) );

            gots.add(nextIt);

            return nextIt;
        }

        public void remove() {
            throw new UnsupportedOperationException("unsupported operation!");
        }
    }

}
